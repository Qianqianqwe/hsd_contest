package com.certification;

import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.huawei.agconnect.auth.AGConnectAuth;
import com.huawei.agconnect.auth.AGConnectAuthCredential;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";
    private Button huaweiidButton;
    private TextView result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        result = findViewById(R.id.result_text);
        huaweiidButton = findViewById(R.id.huaweiid_button);
        huaweiidButton.setOnClickListener(view -> AGConnectAuth.getInstance().signIn(MainActivity.this, AGConnectAuthCredential.HMS_Provider)
                .addOnSuccessListener(signInResult -> {
                    String uid = signInResult.getUser().getUid();
                    Log.i(TAG, "Uid: " + uid);
                    result.setText(uid);
                }).addOnFailureListener(e -> Log.e(TAG, "signin failed, error: " + e.getMessage())));
    }
}