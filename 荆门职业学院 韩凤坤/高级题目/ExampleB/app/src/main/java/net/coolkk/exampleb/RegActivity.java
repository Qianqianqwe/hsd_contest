package net.coolkk.exampleb;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

import com.huawei.agconnect.applinking.AGConnectAppLinking;
import com.huawei.agconnect.auth.AGConnectUser;
import net.coolkk.exampleb.data.User;
import net.coolkk.exampleb.util.Tool;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class RegActivity extends AppCompatActivity {

    private AGConnectUser auth = null;
    private User userShare = new User();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reg);
        //匿名登录
        auth = Tool.login();
        //处理被调用的数据
        AGConnectAppLinking.getInstance().getAppLinking(this).addOnSuccessListener(resolvedLinkData -> {
            if (resolvedLinkData != null) {
                //获取Link
                String deepLink = resolvedLinkData.getDeepLink().toString();
                //解析Link
                userShare.setId(parseLink(deepLink).get("id"));
                userShare.setName(parseLink(deepLink).get("name"));
                userShare.setScore(Integer.valueOf(Objects.requireNonNull(parseLink(deepLink).get("price"))));
            }
        });
        //新用户注册
        Button button = findViewById(R.id.buttonReg);
        button.setOnClickListener(v -> {
            //排除自己
            if (userShare.getId().equals(auth.getUid())) {
                Tool.toast(this, "不能给自己使用");
                return;
            }
            //切换MainActivity
            Intent intent = new Intent();
            intent.setClass(RegActivity.this, MainActivity.class);
            intent.putExtra("userNewId",auth.getUid());
            intent.putExtra("userNewName",auth.getUid());
            intent.putExtra("userNewScore",userShare.getScore());
            intent.putExtra("userShareId",userShare.getId());
            intent.putExtra("userShareName",userShare.getId());
            intent.putExtra("userShareScore",userShare.getScore());
            startActivity(intent);
        });
    }

    private static Map<String, String> parseLink(String url) {
        Map<String, String> myMap = new HashMap<>();
        String[] urlParts = url.split("\\?");
        String[] params = urlParts[1].split("&");
        for (String param : params) {
            String[] keyValue = param.split("=");
            myMap.put(keyValue[0], keyValue[1]);
        }
        return myMap;
    }
}