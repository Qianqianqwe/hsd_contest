package net.coolkk.exampleb.util;

import android.content.Context;
import android.widget.Toast;

import com.huawei.agconnect.auth.AGConnectAuth;
import com.huawei.agconnect.auth.AGConnectUser;

public class Tool {
    public static AGConnectUser login() {
        final AGConnectUser[] auth = {AGConnectAuth.getInstance().getCurrentUser()};
        if (auth[0] == null) {
            AGConnectAuth.getInstance().signInAnonymously()
                    .addOnSuccessListener(signInResult -> {
                        auth[0] = signInResult.getUser();
                    });
        }
        return auth[0];
    }

    public static void toast(Context context, String content) {
        Toast.makeText(context, content, Toast.LENGTH_SHORT).show();
    }
}
