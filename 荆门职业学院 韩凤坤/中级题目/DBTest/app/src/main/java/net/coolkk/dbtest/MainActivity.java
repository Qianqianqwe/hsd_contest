package net.coolkk.dbtest;

import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

import com.huawei.agconnect.AGConnectInstance;
import com.huawei.agconnect.auth.AGConnectAuth;
import com.huawei.agconnect.auth.AGConnectUser;
import com.huawei.agconnect.cloud.database.AGConnectCloudDB;
import com.huawei.agconnect.cloud.database.CloudDBZone;
import com.huawei.agconnect.cloud.database.CloudDBZoneConfig;
import com.huawei.agconnect.cloud.database.CloudDBZoneObjectList;
import com.huawei.agconnect.cloud.database.CloudDBZoneQuery;
import com.huawei.agconnect.cloud.database.CloudDBZoneSnapshot;
import com.huawei.agconnect.cloud.database.exceptions.AGConnectCloudDBException;
import com.huawei.hmf.tasks.Task;

import net.coolkk.dbtest.data.Course;
import net.coolkk.dbtest.data.CourseAdapter;
import net.coolkk.dbtest.data.ObjectTypeInfoHelper;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

public class MainActivity extends AppCompatActivity {

    private AGConnectUser auth = null;
    private CloudDBZone mCloudDBZone = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //匿名登录
        auth = AGConnectAuth.getInstance().getCurrentUser();
        if (auth == null) {
            AGConnectAuth.getInstance().signInAnonymously()
                    .addOnSuccessListener(signInResult -> {
                        auth = signInResult.getUser();
                    });
        }
        //数据库-初始化
        try {
            //初始化
            AGConnectCloudDB.initialize(this);
            //实例
            AGConnectInstance instance = AGConnectInstance.getInstance();
            AGConnectCloudDB mCloudDB = AGConnectCloudDB.getInstance(instance, AGConnectAuth.getInstance(instance));
            mCloudDB.createObjectType(ObjectTypeInfoHelper.getObjectTypeInfo());
            //配置
            CloudDBZoneConfig mConfig = new CloudDBZoneConfig("QuickStartDemo", CloudDBZoneConfig.CloudDBZoneSyncProperty.CLOUDDBZONE_CLOUD_CACHE, CloudDBZoneConfig.CloudDBZoneAccessProperty.CLOUDDBZONE_PUBLIC);
            mConfig.setPersistenceEnabled(true);
            Task<CloudDBZone> openDBZoneTask = mCloudDB.openCloudDBZone2(mConfig, true);
            openDBZoneTask.addOnSuccessListener(cloudDBZone -> {
                mCloudDBZone = cloudDBZone;
            });
        } catch (AGConnectCloudDBException e) {
            e.printStackTrace();
        }
        //数据库-操作-添加
        Button buttonAdd = findViewById(R.id.buttonAdd);
        buttonAdd.setOnClickListener(v -> {
            if (mCloudDBZone == null || auth == null) return;
            //新课程
            Course courseNew = new Course();
            courseNew.setId(String.valueOf(new Date().getTime()));
            courseNew.setName("Course:" + getSmallLetter(4));
            courseNew.setTeacher("Teacher:" + getLargeLetter());
            courseNew.setPlace("ClassRoom:" + getNumLargeLetter(5));
            //提交数据
            Task<Integer> upsertTaskNew = mCloudDBZone.executeUpsert(courseNew);
            upsertTaskNew.addOnSuccessListener(cloudDBZoneResult -> Log.d("storageLog", "courseNew")).addOnFailureListener(e -> Log.d("storageLog", e.getMessage()));
        });
        //数据库-操作-刷新
        Button buttonRefresh = findViewById(R.id.buttonRefresh);
        buttonRefresh.setOnClickListener(v -> {
            if (mCloudDBZone == null || auth == null) return;
            //查询数据
            Task<CloudDBZoneSnapshot<Course>> queryTask = mCloudDBZone.executeQuery(
                    CloudDBZoneQuery.where(Course.class),
                    CloudDBZoneQuery.CloudDBZoneQueryPolicy.POLICY_QUERY_FROM_CLOUD_ONLY);
            queryTask.addOnSuccessListener(snapshot -> {
                //数据
                List<Course> courseList = new ArrayList<>();
                //处理
                CloudDBZoneObjectList<Course> courseData = snapshot.getSnapshotObjects();
                for (int i = 0; i < courseData.size(); i++) {
                    try {
                        Course course = courseData.get(i);
                        courseList.add(course);
                    } catch (AGConnectCloudDBException e) {
                        e.printStackTrace();
                    }
                }
                //设置
                CourseAdapter courseAdapter = new CourseAdapter(this, R.layout.listview_main, courseList);
                ListView listView = (ListView) findViewById(R.id.listView);
                listView.setAdapter(courseAdapter);
            });
        });
    }

    /**
     * 生成随机小写字母字符串
     */
    public static String getSmallLetter(int size) {
        StringBuilder buffer = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < size; i++) {
            buffer.append((char) (random.nextInt(27) + 'a'));
        }
        return buffer.toString();
    }

    /**
     * 生成随机大写字母
     */
    public static String getLargeLetter() {
        Random random = new Random();
        return String.valueOf((char) (random.nextInt(27) + 'A'));
    }

    /**
     * 数字与大写字母混编字符串
     */
    public static String getNumLargeLetter(int size) {
        StringBuilder buffer = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < size; i++) {
            if (random.nextInt(2) % 2 == 0) {//字母
                buffer.append((char) (random.nextInt(27) + 'A'));
            } else {//数字
                buffer.append(random.nextInt(10));
            }
        }
        return buffer.toString();
    }
}