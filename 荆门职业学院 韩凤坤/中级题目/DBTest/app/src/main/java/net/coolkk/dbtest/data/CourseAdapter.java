package net.coolkk.dbtest.data;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import net.coolkk.dbtest.R;

import java.util.List;

public class CourseAdapter extends ArrayAdapter<Course> {

    private int resourceId;

    public CourseAdapter(Context context, int textViewResourceId, List<Course> objects) {
        super(context, textViewResourceId, objects);
        resourceId = textViewResourceId;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Course course = getItem(position);
        @SuppressLint("ViewHolder") View view = LayoutInflater.from(getContext()).inflate(resourceId, parent, false);
        TextView courseName = (TextView) view.findViewById(R.id.textViewCourse);
        TextView coursePlace = (TextView) view.findViewById(R.id.textViewPlace);
        courseName.setText(course.getName() + " @ " + course.getTeacher());
        coursePlace.setText(course.getPlace());
        return view;
    }
}